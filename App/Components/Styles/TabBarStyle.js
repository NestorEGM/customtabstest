import { StyleSheet } from 'react-native'
import { Colors, Metrics } from '../../Themes';

export const widthButton = 175;
export const heightButton = 30;
export const sizeMainButton = 75;
export const heightContainer = 85;

export const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: heightContainer,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tab: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButton: {
    color: Colors.snow,
  },
  mainButton: {
    borderRadius: sizeMainButton/2,
    backgroundColor: Colors.mainButton,
    width: sizeMainButton,
    height: sizeMainButton,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: Metrics.screenWidth/2,
    top: heightContainer/2,
    transform: [
      {
        translateX: -sizeMainButton/2
      },
      {
        translateY: -sizeMainButton/2
      }
    ],
    overflow: 'hidden',
  },
  textMainButton: {
    color: Colors.snow,
    fontWeight: 'bold',
    fontSize: 18,
  },
})
