import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { View, Text, Animated } from 'react-native'
import { connect } from 'react-redux'
import styles from './Styles/ScreenTransitionStyle'
import { Metrics } from '../Themes';

class ScreenTransition extends Component {

  // Prop type warnings
  static propTypes = {
    toRight: PropTypes.bool,
  }
  
  // Defaults for props
  static defaultProps = {}

  animatedValue = new Animated.Value(0);
  animateParams = this.inputAnimateParams();

  componentDidMount(){
    this.startAnimation();
  }

  componentDidUpdate(prevProps, prevState){
    if(prevProps.routeActive !== this.props.routeActive){
      if(this.props.routeActive === this.props.screen){
        this.startAnimation();
      }
      // if(prevProps.routeActive === this.props.screen){
      //   animateParams = this.outputAnimateParams();
      //   this.startAnimation();
      // }
    }
  }

  startAnimation(){
    this.animatedValue.setValue(0);
    Animated.timing(this.animatedValue, {
      toValue: 1,
      useNativeDriver: true,
      duration: 500,
    }).start();
  }

  inputAnimateParams(){
    const { toRight } = this.props;
    let params = {
      left: Metrics.screenWidth - 1,
      transform: [
        {
          translateX: this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -Metrics.screenWidth + 1]
          })
        }
      ],
    }
    if(toRight){
      params = {
        left: -Metrics.screenWidth + 1,
        transform: [
          {
            translateX: this.animatedValue.interpolate({
              inputRange: [0, 1],
              outputRange: [0, Metrics.screenWidth - 1]
            })
          }
        ],
      }
    }
    return params;
  }

  outputAnimateParams(){
    const { toRight } = this.props;
    let params = {
      left: 0,
      transform: [
        {
          translateX: this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [0, Metrics.screenWidth],
          })
        }
      ]
    }
    if(toRight){
      params = {
        left: 0,
        transform: [
          {
            translateX: this.animatedValue.interpolate({
              inputRange: [0, 1],
              outputRange: [0, -Metrics.screenWidth],
            })
          }
        ]
      }
    }
    return params;
  }

  render () {
    const { children } = this.props;
    return (
      <Animated.View style={{
        position: 'absolute',
        top: 0,
        height: Metrics.screenHeight,
        width: Metrics.screenWidth,
        ...this.animateParams,
      }}>
        { children }
      </Animated.View>
    )
  }
}

const mapStateToProps = ({ nav }) => {
  return {
    routeActive: nav.routes[nav.index].key,
    routes: nav.routes
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ScreenTransition)
