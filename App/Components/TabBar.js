import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text } from 'react-native'
import { styles, widthButton, heightButton, sizeMainButton } from './Styles/TabBarStyle'
import { Svg, Path, } from 'react-native-svg';
import Ripple from 'react-native-material-ripple';

export default class TabBar extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    const { getLabelText, activeTintColor, inactiveTintColor, onTabPress, navigation } = this.props;
    const { routes, index: activeRouteIndex } = navigation.state;
    const routes1 = [ routes[0], routes[1] ];
    const routes2 = [ routes[2], routes[3] ];
    const cut1 = 20;
    const padding = 45;
    const radio = sizeMainButton/2;
    const cateto = Math.sqrt(Math.pow(radio, 2) - Math.pow(heightButton, 2)) - 1;
    const space = 3;
    const paramsPolygon1 = [ { d: `M0 0 L${widthButton - cateto - space} 0 A${radio} ${radio} 0 0 0 ${widthButton - radio - space} ${heightButton} L${cut1} ${heightButton} Z` }, { d: `M${cateto + space} 0 L${widthButton} 0 L${widthButton - cut1} ${heightButton} L${radio + space} ${heightButton} A${radio} ${radio} 0 0 0  ${cateto + space} 0` } ];
    const paramsPolygon2 = [ { d: `M${cut1} 0 L${widthButton - radio - space} 0 A${radio} ${radio} 0 0 0 ${widthButton - cateto - space} ${heightButton} L${cut1 * 2} ${heightButton} Z` }, { d: `M${radio + space} 0 L${widthButton - cut1} 0 L${widthButton - cut1 * 2} ${heightButton} L${cateto + space} ${heightButton} A${radio} ${radio} 0 0 0 ${radio + space} 0 Z` } ];
    const customStyle = [ { paddingRight: padding, textAlign: 'right', height: heightButton, textAlignVertical: 'center' }, { paddingLeft: padding, textAlign: 'left', height: heightButton, textAlignVertical: 'center' }, ];
    return (
      <View style={ styles.container }>
        <View style={ [ styles.rowContainer, { paddingBottom: 3 } ] } >
          {
            routes1.map((route, routeIndex) => {
              const isRouteActive = routeIndex === activeRouteIndex;
              const tintColor = isRouteActive ? activeTintColor : inactiveTintColor;

              return (
                <Ripple
                  key={ routeIndex }
                  onPress={ () => { onTabPress({ route }) } }
                >
                  <Svg
                    width={ widthButton }
                    height={ heightButton }
                  >
                    <Path
                      fill={ tintColor }
                      { ...paramsPolygon1[routeIndex] }
                    />
                    <Text style={ [ styles.textButton, customStyle[routeIndex] ] }> { getLabelText({ route }) } </Text>
                  </Svg>
                </Ripple>
              );
            })
          }
        </View>
        <View style={styles.rowContainer} >
          {
            routes2.map((route, routeIndex) => {
              const isRouteActive = routeIndex + 2 === activeRouteIndex;
              const tintColor = isRouteActive ? activeTintColor : inactiveTintColor;

              return (
                <Ripple
                  key={ routeIndex }
                  onPress={ () => { onTabPress({ route }) } }
                >
                  <Svg
                    width={ widthButton }
                    height={ heightButton }
                  >
                    <Path
                      fill={ tintColor }
                      { ...paramsPolygon2[routeIndex] }
                    />
                    <Text  style={ [ styles.textButton, customStyle[routeIndex] ] }> { getLabelText({ route }) } </Text>
                  </Svg>
                </Ripple>
              );
            })
          }
        </View>
        <Ripple onPress={ () => {} } style={ styles.mainButton } rippleCentered={ true }>
          <Text style={ styles.textMainButton }>INLETE</Text>
        </Ripple>
      </View>
    )
  }
}
