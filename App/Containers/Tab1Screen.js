import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/Tab1ScreenStyle'

//Component
import ScreenTransition from '../Components/ScreenTransition';

class Tab1Screen extends Component {
  render () {
    return (
      <ScreenTransition toRight={ true } screen={ this.props.navigation.state.key }>
        <View style={styles.container}>
          <Text>Tab1Screen</Text>
        </View>
      </ScreenTransition>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Tab1Screen)
