import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/Tab2ScreenStyle'

//Components
import ScreenTransition from '../Components/ScreenTransition';

class Tab2Screen extends Component {
  render () {
    return (
      <ScreenTransition toRight={ false } screen={ this.props.navigation.state.key }>
        <ScrollView style={styles.container}>
          <KeyboardAvoidingView behavior='position'>
            <Text>Tab2Screen</Text>
          </KeyboardAvoidingView>
        </ScrollView>
      </ScreenTransition>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Tab2Screen)
