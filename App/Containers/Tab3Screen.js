import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/Tab3ScreenStyle'

//Components
import ScreenTransition from '../Components/ScreenTransition';

class Tab3Screen extends Component {
  render () {
    return (
      <ScreenTransition toRight={ true } screen={ this.props.navigation.state.key }>
        <ScrollView style={styles.container}>
          <KeyboardAvoidingView behavior='position'>
            <Text>Tab3Screen</Text>
          </KeyboardAvoidingView>
        </ScrollView>
      </ScreenTransition>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Tab3Screen)
