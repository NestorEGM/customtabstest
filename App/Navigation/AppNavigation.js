import { createBottomTabNavigator, createStackNavigator, createAppContainer } from 'react-navigation'
import LaunchScreen from '../Containers/LaunchScreen'
import Tab1Screen from '../Containers/Tab1Screen'
import Tab2Screen from '../Containers/Tab2Screen'
import Tab3Screen from '../Containers/Tab3Screen'
import TabBar from '../Components/TabBar';

import styles from './Styles/NavigationStyles'
import { Colors } from '../Themes';

// Manifest of possible screens
const PrimaryNav = createBottomTabNavigator({
  Tab1: { screen: Tab1Screen },
  Tab2: { screen: Tab2Screen },
  Tab3: { screen: Tab3Screen },
  LaunchScreen: { screen: LaunchScreen },
}, {
  // Default config for all screens
  // headerMode: 'none',
  initialRouteName: 'Tab1',
  navigationOptions: {
    headerStyle: styles.header
  },
  tabBarComponent: TabBar,
  tabBarOptions: {
    activeTintColor: Colors.activeTab,
    inactiveTintColor: Colors.inactiveTab,
  },
})

// export default createAppContainer(PrimaryNav)
export default PrimaryNav;
